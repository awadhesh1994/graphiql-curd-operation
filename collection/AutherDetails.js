const mongo = require("mongoose");
const blogautherschema = mongo.Schema;

const BlogAutherFields = new blogautherschema({
    autherName: String,
    autherEmail: String,
    autherPhone: String,
    autherWorkingField: String,
    autherNumberPost: Number
});

module.exports = mongo.model("blogautherfields", BlogAutherFields);