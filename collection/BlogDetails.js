const mongo = require("mongoose");
const blogdetailschema = mongo.Schema;

const BlogFields = new blogdetailschema({
    blogTitle: String,
    blogSubTitle: String,
    blogAutherId: String,
    blogCategory: String,
    blogSubDetails: String,
    blogAllDetails: String,
    blogPostDateTime: String,
    blogWebsiteLink: String,
    blogRating: Number,
    blogImageOne: String,
    blogImageTwo: String,
    blogImageThree: String
});

module.exports = mongo.model("blogfields", BlogFields);