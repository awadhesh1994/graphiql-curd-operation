const {
	GraphQLInt,
	GraphQLString,
	GraphQLNonNull,
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLList,
	GraphQLID
} = require("graphql");
const axios = require('axios');

const blogPostFields = require("../collection/BlogDetails");
const blogAutherFields = require("../collection/AutherDetails");

const BlogPostType = new GraphQLObjectType({
	name: "BlogPost",
	fields: () => ({
		id: {type:GraphQLID},
		blogTitle: {type:GraphQLString},
		blogSubTitle: {type:GraphQLString},
		blogAutherId: {type: GraphQLString},
		blogCategory: {type:GraphQLString},
		blogPostDateTime: {type:GraphQLString},
		blogSubDetails: {type:GraphQLString},
		blogAllDetails: {type:GraphQLString},
		blogWebsiteLink: {type:GraphQLString},
		blogRating: {type:GraphQLInt},
		blogImageOne: {type: GraphQLString},
		blogImageTwo: {type: GraphQLString},
		blogImageThree: {type: GraphQLString},
		AutherDetails : {
			type: BlogAutherType,
			resolve(parent, args){
				return blogAutherFields.findById(parent.blogAutherId);
			}
		}
	})
});

const BlogAutherType = new GraphQLObjectType({
	name: "BlogPostAuther",
	fields: () => ({
		id: {type: GraphQLID},
		autherName: {type:GraphQLString},
		autherEmail: {type:GraphQLString},
		autherPhone: {type:GraphQLString},
		autherWorkingField: {type:GraphQLString},
		autherNumberPost: {type:GraphQLInt},
		AutherPosts : {
			type: new GraphQLList(BlogPostType),
			resolve(parent, args){
				return blogPostFields.find({blogAutherId: parent.id});
			}
		}
	})
});

const RootQuery = new GraphQLObjectType({
	name: "RootQuery",
	fields: {
		AllBlogPost : {
			type: new GraphQLList(BlogPostType),
			resolve(parent, args){	
				return blogPostFields.find({});
			}
		},
		FindBlogPost : {
			type: BlogPostType,
			args: {
				id: {type: GraphQLID}
			},
			resolve(parent, args){
				return blogPostFields.findById(args.id);
			}
		},
		AllAuther : {
			type: new GraphQLList(BlogAutherType),
			resolve(parent, args){
				return blogAutherFields.find({});
			}
		},
		FindAutherPost : {
			type: BlogAutherType,
			args: {
				id: {type: GraphQLID}
			},
			resolve(parent, args){
				return blogAutherFields.findById(args.id);
			}
		}
	}
});

const MutationQuery = new GraphQLObjectType({
	name: "MutationQuery",
	fields: {
		AddBlogPost: {
			type: BlogPostType,
			args: {
				blogTitle: {type:GraphQLString},
				blogSubTitle: {type:GraphQLString},
				blogAutherId: {type: GraphQLString},
				blogCategory: {type:GraphQLString},
				blogPostDateTime: {type:GraphQLString},
				blogSubDetails: {type:GraphQLString},
				blogAllDetails: {type:GraphQLString},
				blogWebsiteLink: {type:GraphQLString},
				blogRating: {type:GraphQLInt}
			},
			resolve(parent, args){
				let BPFSave = new blogPostFields({
					blogTitle: args.blogTitle,	
					blogSubTitle: args.blogSubTitle,
					blogAutherId: args.blogAutherId,
					blogCategory: args.blogCategory,
					blogPostDateTime: args.blogPostDateTime,
					blogSubDetails: args.blogSubDetails,
					blogAllDetails: args.blogAllDetails,
					blogWebsiteLink: args.blogWebsiteLink,
					blogRating: args.blogRating
				});
				return BPFSave.save();
			}
		},
		AddAutherDetails : {
			type: BlogAutherType,
			args : {
				autherName: {type:GraphQLString},
				autherEmail: {type:GraphQLString},
				autherPhone: {type:GraphQLString},
				autherWorkingField: {type:GraphQLString},
				autherNumberPost: {type:GraphQLInt}
			},
			resolve(parent, args){
				let ADSave = new blogAutherFields({
					autherName: args.autherName,
					autherEmail: args.autherEmail,
					autherPhone: args.autherPhone,
					autherWorkingField: args.autherWorkingField,
					autherNumberPost: args.autherNumberPost
				});	
				return ADSave.save();
			}
		},
		DeleteBlogPost: {
			type: BlogPostType,
			args: {
				id: {type: GraphQLID}
			},
			resolve(parent, args){
				return blogPostFields.findByIdAndRemove(args.id).exec();
			}
		}
	}
});

module.exports = new GraphQLSchema({
	query:RootQuery,
	mutation:MutationQuery
});

