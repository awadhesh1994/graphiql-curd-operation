const express = require("express");
const app = express();
const graphqlHTTP = require("express-graphql");
const mlabSchema = require("./schema/mongoSchema");
const mong = require("mongoose");
const PORT = process.env.PORT || 5000;
const cors = require("cors");

app.use(cors());

mong.connect("mongodb://awadhesh:toor12345@ds123534.mlab.com:23534/gql-awadhesh", () => {
	console.log("Your Know Connected With MLab :-)) ");
});

app.use("/", graphqlHTTP({
	schema:mlabSchema,
	graphiql: true
}));

app.listen(PORT, () => {
	console.log(`your server running at ${PORT}.`);
})
