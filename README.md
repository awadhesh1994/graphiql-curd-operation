<p align="center">
  <a href="https://getbootstrap.com/">
    <img src="https://graphql.org/img/logo.svg" alt=GraphQL" width="100" height="100">
  </a>
</p>

<h2 align="center">GraphQL</h2>

### A query language for your API

GraphQL is a query language for APIs and a runtime for fulfilling those queries with your existing data. GraphQL provides a complete and understandable description of the data in your API, gives clients the power to ask for exactly what they need and nothing more, makes it easier to evolve APIs over time, and enables powerful developer tools. Send a GraphQL query to your API and get exactly what you need, nothing more and nothing less. GraphQL queries always return predictable results. Apps using GraphQL are fast and stable because they control the data they get, not the server.
<br/><br>
[![npm version](https://badge.fury.io/js/graphql.svg)](https://badge.fury.io/js/graphql)
[![Build Status](https://travis-ci.org/graphql/graphql-js.svg?branch=master)](https://travis-ci.org/graphql/graphql-js?branch=master)
[![Coverage Status](https://codecov.io/gh/graphql/graphql-js/branch/master/graph/badge.svg)](https://codecov.io/gh/graphql/graphql-js)

<h1><a href="https://techbrain-server.herokuapp.com/">DEMO</a></h1>

### Table Contains 

- AllBlogPost
- AllAuther

### Curd Operation

- AddBlogPost
- AddAutherDetails
- DeleteBlogPost
- FindBlogPost
- FindAutherPost



